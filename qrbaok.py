import os
import pyqrcode
import pandas as pd
base_dir  = os.path.dirname(os.path.realpath(__file__))
df = pd.read_excel(f'{base_dir}/data.xlsx')
path = input("Buat folder QR: ")
new_path = os.path.join(base_dir, path)
if os.path.isdir(new_path):
    print(f"Folder {path} sudah ada, atau hapus sebelumnya")

else:
    os.mkdir(new_path)
    for index, item in df[0:2].iterrows():
        data = f"NAMA     : {item['NAMA']}\nNIK          : {item['NIK']}\nALAMAT : {item['ALAMAT']}\n\nJABATAN: {item['JABATAN']}\nDPC         : {item['DPC']}"
        url = pyqrcode.create(data)
        filename = f"{item['NAMA']}.png"
        fullpath = os.path.join(new_path, filename)
        url.png(fullpath, scale=6)

    print("Tugas telah selesai")